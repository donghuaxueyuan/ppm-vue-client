import axios from 'axios'
import {MessageBox, Message} from 'element-ui'

// 创建实例时设置配置的默认值
const instance = axios.create({

    baseURL: process.env.VUE_APP_BASE_API,
    // 跨域请求时，是否发送cookies
    withCredentials: false,

    // 超时时间
    timeout: 5000
});

// // request 拦截器
// instance.interceptors.request.use(
//     config => {
//         return config
//     },
//     error => {
//         console.log("request: " + error);
//         return Promise.reject(error);
//     }
// );
//
// // response 拦截器
// instance.interceptors.response.use(
//     response => {
//         const data = response.data;
//
//         // TODO 请求错误，统一处理
//         if (data.code !== 20000) {
//
//             // if (data.code === 50008 || data.code === 50012 || data.code === 50014) {
//             //
//             //     MessageBox.confirm('Your have been logged out.', {
//             //         confirmButtonText: '重新登录',
//             //         cancelButtonText: '取消',
//             //         type: 'warning'
//             //     }).then(() => {
//             //
//             //     })
//             // }
//
//             return Promise.reject(new Error(data.message || 'Error'))
//         }
//
//         // api报错
//         // if (!data.data.success) {
//         //     // TODO 显示 or 弹出错误提示信息
//         //     alert(data.msg)
//         //
//         //     // TODO 未登录 or 登录超时，踢到登录页面
//         //     if (!data.data.token) {
//         //
//         //     }
//         // }
//
//         // 只要api请求处理成功，返回数据，各个api可以继续处理
//         return data;
//
//     },
//     error => {
//         console.log(error);
//         return Promise.reject(error);
//     }
// );

export default instance
