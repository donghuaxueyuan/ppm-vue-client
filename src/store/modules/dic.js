
const dic = {}

dic.state = {
    list: [
        { id: 1, "username": "Tom", done: true },
        { id: 2, "username": "Jerry", done: false }
    ]
}

dic.getters = {

}

dic.mutations = {

}

dic.actions = {

}

export default dic
