
//import layout from '@/view/layout'
const BasicLayout = () => import('@/views/layout/BasicLayout')

const asyncRoutes = [
    {
        path: '/system',
        component: BasicLayout,
        children: [
            {
                path: 'about',
                name: 'about',
                component: () => import('@/views/About'),
                meta: {title: 'About', icon: 'about'}
            },
            {
                path: 'user',
                name: 'user',
                component: () => import('@/views/system/user'),
                meta: {title: 'user', icon: 'user'}
            }
        ]
    },
  { path: '*', hidden: true, redirect: '/404' }
]

export default asyncRoutes
