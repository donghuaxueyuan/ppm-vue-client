'use strict'
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const BasicLayout = () => import('@/views/layout/BasicLayout')
export const asyncRoutes = require('./asyncRoutes').default

export const constantRoutes = [
    {
        path: '/login',
        component: () => import('@/views/login/index'),
        hidden: true,
        name: 'login'
    },
    {
        path: '/auth-redirect',
        component: () => import('@/views/login/auth-redirect'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/error-page/404'),
        hidden: true
    },
    {
        path: '/401',
        component: () => import('@/views/error-page/401'),
        hidden: true
    },
    {
        path: '/',
        component: BasicLayout,
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                component: () => import('@/views/dashboard/index'),
                name: 'Dashboard',
                meta: { title: 'Dashboard', icon: 'dashboard', affix: true }
            }
        ]
    },
    // {
    //     path: '/documentation',
    //     component: Layout,
    //     children: [
    //         {
    //             path: 'index',
    //             component: () => import('@/views/documentation/index'),
    //             name: 'Documentation',
    //             meta: { title: 'Documentation', icon: 'documentation', affix: true }
    //         }
    //     ]
    // },
    // {
    //     path: '/guide',
    //     component: Layout,
    //     redirect: '/guide/index',
    //     children: [
    //         {
    //             path: 'index',
    //             component: () => import('@/views/guide/index'),
    //             name: 'Guide',
    //             meta: { title: 'Guide', icon: 'guide', noCache: true }
    //         }
    //     ]
    // },
    {
        path: '/profile',
        component: BasicLayout,
        redirect: '/profile/index',
        hidden: true,
        children: [
            {
                path: 'index',
                component: () => import('@/views/system/user/index'),
                name: 'Profile',
                meta: { title: 'Profile', icon: 'user', noCache: true }
            }
        ]
    }
]


const createRouter = () => new Router({
    mode: 'history', // require service support
    //scrollBehavior: () => ({ y: 0 }),
    //base: process.env.BASE_URL,
    routes: constantRoutes
})

const router = createRouter()

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// 通过addRoutes添加登录后有权限的路由，但是退出登录后，另一个用户登录还是上一个的权限路由的问题

export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router


