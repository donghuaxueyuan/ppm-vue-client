
import Mock from 'mockjs'

export default () => Mock.mock(() => {
  return {
      code: 20000,
      data: {
          success: true,
          msg: "登出成功！"
      }
  }
})
