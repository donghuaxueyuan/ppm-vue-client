
import Mock from 'mockjs'

export default params => Mock.mock(() => {
  if (params.token === 'hsghd23h2g3j2') {
    return {
      code: 1,
      msg: 'success',
      success: true,
      data: {
          name: 'user123',
          role: 'user',
          userId: 1,
          password: "$2a$10$llOd5/mvInD.OTVkPr/hqeFaa6wX4DCbtKIJwRq5vhKLd3mJNtLqi",
          nation: 86,
          phone: 15985859933,
          avatar: null,
          email: "test@test.com",
          gender: null,
          birthday: "2018-07-16T00:50:31.000+0000",
          enabled: false,
          type: 1,
          createDate: "2018-07-16T00:50:31.000+0000",
          lastDate: "2018-07-16T00:50:31.000+0000",
          signInDate: "2018-07-16T00:50:31.000+0000"
      }
    }
  } else if (params.token === 'hasjdah2h1jjh') {
    return {
      code: 1,
      success: true,
      msg: 'success',
      data: {
        name: 'admin123',
        role: 'admin',
          userId: 2,
          password: "$2a$10$llOd5/mvInD.OTVkPr/hqeFaa6wX4DCbtKIJwRq5vhKLd3mJNtLqi",
          nation: 86,
          phone: 15985859933,
          avatar: null,
          email: "test@test.com",
          gender: null,
          birthday: "2018-07-16T00:50:31.000+0000",
          enabled: true,
          type: 1,
          createDate: "2018-07-16T00:50:31.000+0000",
          lastDate: "2018-07-16T00:50:31.000+0000",
          signInDate: "2018-07-16T00:50:31.000+0000"
      }
    }
  } else if (params.token === 'sdah4kh3k4jh3') {
    return {
      code: 1,
      msg: 'success',
      success: true,
      data: {
        name: 'superadmin123',
        role: 'superadmin',
          userId: 3,
          password: "$2a$10$llOd5/mvInD.OTVkPr/hqeFaa6wX4DCbtKIJwRq5vhKLd3mJNtLqi",
          nation: 86,
          phone: 15985859933,
          avatar: null,
          email: "test@test.com",
          gender: null,
          birthday: "2018-07-16T00:50:31.000+0000",
          enabled: true,
          type: 1,
          createDate: "2018-07-16T00:50:31.000+0000",
          lastDate: "2018-07-16T00:50:31.000+0000",
          signInDate: "2018-07-16T00:50:31.000+0000"
      }
    }
  } else {
    return {
      code: 0,
      msg: 'token无效',
      success: false
    }
  }
})
