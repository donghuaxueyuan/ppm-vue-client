
import Mock from 'mockjs'

export default params => Mock.mock(() => {
  const { username } = params
  const result = {
      code: 20000,
      success: true,
      msg: "登录成功！",
      data: {

              menuTree: [
                  {
                      menuId: 1,
                      menuName: "根节点",
                      menuCode: "root",
                      link: "/root",
                      icon: "el-icon-menu",
                      lft: 1,
                      rgt: 14,
                      depth: 0
                  },
                  {
                      menuId: 2,
                      menuName: "首页",
                      menuCode: "dashboard",
                      link: "/dashboard",
                      icon: "el-icon-menu",
                      lft: 2,
                      rgt: 3,
                      depth: 1
                  },
                  {
                      menuId: 3,
                      menuName: "系统管理",
                      menuCode: "system",
                      link: "/system",
                      icon: "el-icon-menu",
                      lft: 4,
                      rgt: 9,
                      depth: 1
                  },
                  {
                      menuId: 4,
                      menuName: "个人设定",
                      menuCode: "profile",
                      link: "/system/profile",
                      icon: "el-icon-menu",
                      lft: 5,
                      rgt: 6,
                      depth: 2
                  },
                  {
                      menuId: 5,
                      menuName: "用户管理",
                      menuCode: "user",
                      link: "/system/user",
                      icon: "el-icon-menu",
                      lft: 5,
                      rgt: 6,
                      depth: 2
                  },
                  {
                      menuId: 6,
                      menuName: "角色管理",
                      menuCode: "role",
                      link: "/system/role",
                      icon: "el-icon-menu",
                      lft: 7,
                      rgt: 8,
                      depth: 2
                  },
                  {
                      menuId: 7,
                      menuName: "主数据管理",
                      menuCode: "mdm",
                      link: "/mdm",
                      icon: "el-icon-menu",
                      lft: 10,
                      rgt: 13,
                      depth: 1
                  },
                  {
                      menuId: 8,
                      menuName: "业务字典",
                      menuCode: "dict",
                      link: "/mdm/dict",
                      icon: "el-icon-menu",
                      lft: 11,
                      rgt: 12,
                      depth: 2
                  }
              ]

      }
  }

  if (username === 'user123') {
    result.data.token = 'hsghd23h2g3j2'
  } else if (username === 'admin') {
    result.data.token = 'hasjdah2h1jjh'
  } else if (username === 'superadmin') {
    result.data.token = 'sdah4kh3k4jh3'
  } else {
    result.code = 0
    result.data.msg = '用户密码错误'
  }

  return result
})
