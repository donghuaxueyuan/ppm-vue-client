import Mock from 'mockjs'

const url = '/users/'
let sequence = 100

const List = [
    {
        userId: 1,
        username: "guest1",
        password: "$2a$10$llOd5/mvInD.OTVkPr/hqeFaa6wX4DCbtKIJwRq5vhKLd3mJNtLqi",
        nation: 86,
        phone: 15985859933,
        avatar: null,
        email: "test@test.com",
        gender: null,
        birthday: "2018-07-16T00:50:31.000+0000",
        enabled: false,
        type: 1,
        createDate: "2018-07-16T00:50:31.000+0000",
        lastDate: "2018-07-16T00:50:31.000+0000",
        signInDate: "2018-07-16T00:50:31.000+0000"
    },
    {
        userId: 2,
        username: "user1",
        password: "$2a$10$llOd5/mvInD.OTVkPr/hqeFaa6wX4DCbtKIJwRq5vhKLd3mJNtLqi",
        nation: 86,
        phone: 15985859933,
        avatar: null,
        email: "test@test.com",
        gender: null,
        birthday: "2018-07-16T00:50:31.000+0000",
        enabled: true,
        type: 1,
        createDate: "2018-07-16T00:50:31.000+0000",
        lastDate: "2018-07-16T00:50:31.000+0000",
        signInDate: "2018-07-16T00:50:31.000+0000"
    },
    {
        userId: 3,
        username: "guest2",
        password: "$2a$10$llOd5/mvInD.OTVkPr/hqeFaa6wX4DCbtKIJwRq5vhKLd3mJNtLqi",
        nation: 86,
        phone: 15985859933,
        avatar: null,
        email: "test@test.com",
        gender: null,
        birthday: "2018-07-16T00:50:31.000+0000",
        enabled: true,
        type: 1,
        createDate: "2018-07-16T00:50:31.000+0000",
        lastDate: "2018-07-16T00:50:31.000+0000",
        signInDate: "2018-07-16T00:50:31.000+0000"
    }

]

export default [
    {
        url: /^\/users\/?$/,
        type: 'get',
        response: config => {
            let items = List
            const current = config.body.current ? config.body.current : 1
            const size = config.body.size ? config.body.size : 10
            if (config.body.username) {
                items = List.filter( (n) => n.username.indexOf(config.body.username) > -1)
            }

            return {
                code: 20000,
                data: {
                    success: true,
                    data: items.slice((current - 1) * size, current * size),
                    current: current,
                    size: size,
                    total: items.length
                }
            };
        }
    },
    {
        url: /^\/users\/\w+\/?$/,
        type: 'get',
        response: config => {
            const items = config.body.id >= List.length ? {} : List[config.body.id]
            return {
                code: 20000,
                data: {
                    success: true,
                    data: items
                }
            }
        }
    },
    {
        url: /^\/users\/?$/,
        type: 'post',
        response: config => {
            let user = config.body;
            if (user.id === undefined || user.id === '' || user.id == '0') {
                user.id = sequence ++;
                List.push(user)
            }
            else {
                for (let i = 0; i < List.length; i++) {
                    if (List[i].id == user.id) {
                        List[i] = user
                        break
                    }
                }
            }
            return {
                code: 20000,
                data: {
                    success: true,
                    msg: "用户添加成功！"
                }
            }
        }
    },
    {
        url: /^\/users\/\w+\/?$/,
        type: 'put',
        response: config => {
            return {
                code: 20000,
                data: {
                    success: false,
                    msg: "用户名重复！"
                }
            }
        }
    },
    {
        url: /^\/users\/\w+\/?$/,
        type: 'delete',
        response: config => {
            console.log(config)
            //splice
            for (let i = 0; i < List.length; i++) {
                if (List[i].id == config.id) {
                    List.splice(i, 1)
                    break
                }
            }
            return {
                code: 20000,
                data: {
                    success: true,
                    msg: "用户[" + config.body.username + "]删除成功！"
                }
            }
        }
    },
    {
        url: /^\/login\/?$/,
        type: 'post',
        response: config => {
            console.log(config)
            return {
                code: 20000,
                data: {
                    success: true,
                    msg: "登录成功！",
                    data: {
                        menuTree: [
                            {
                                menuId: 1,
                                menuName: "根节点",
                                menuCode: "root",
                                link: "/root",
                                icon: "el-icon-menu",
                                lft: 1,
                                rgt: 14,
                                depth: 0
                            },
                            {
                                menuId: 2,
                                menuName: "首页",
                                menuCode: "dashboard",
                                link: "/dashboard",
                                icon: "el-icon-menu",
                                lft: 2,
                                rgt: 3,
                                depth: 1
                            },
                            {
                                menuId: 3,
                                menuName: "系统管理",
                                menuCode: "system",
                                link: "/system",
                                icon: "el-icon-menu",
                                lft: 4,
                                rgt: 9,
                                depth: 1
                            },
                            {
                                menuId: 4,
                                menuName: "个人设定",
                                menuCode: "profile",
                                link: "/system/profile",
                                icon: "el-icon-menu",
                                lft: 5,
                                rgt: 6,
                                depth: 2
                            },
                            {
                                menuId: 5,
                                menuName: "用户管理",
                                menuCode: "user",
                                link: "/system/user",
                                icon: "el-icon-menu",
                                lft: 5,
                                rgt: 6,
                                depth: 2
                            },
                            {
                                menuId: 6,
                                menuName: "角色管理",
                                menuCode: "role",
                                link: "/system/role",
                                icon: "el-icon-menu",
                                lft: 7,
                                rgt: 8,
                                depth: 2
                            },
                            {
                                menuId: 7,
                                menuName: "主数据管理",
                                menuCode: "mdm",
                                link: "/mdm",
                                icon: "el-icon-menu",
                                lft: 10,
                                rgt: 13,
                                depth: 1
                            },
                            {
                                menuId: 8,
                                menuName: "业务字典",
                                menuCode: "dict",
                                link: "/mdm/dict",
                                icon: "el-icon-menu",
                                lft: 11,
                                rgt: 12,
                                depth: 2
                            }
                        ]
                    }
                }
            }
        }
    },
    {
        url: /^\/logout\/?$/,
        type: 'delete',
        response: config => {
            return {
                code: 20000,
                data: {
                    success: true,
                    msg: "登出成功！"
                }
            }
        }
    }
]
