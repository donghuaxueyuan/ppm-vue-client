import Vue from 'vue'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import "@/styles/app.scss";

import App from './App.vue'
import router from './router'
import store from './store'

//const moment = require("moment");
import moment from 'moment'
require("moment/locale/zh-cn");

import '@/interceptor'

import {mockXHR} from '@/mock'

if (process.env.NODE_ENV === 'development') {
    mockXHR()
}

Vue.use(require("vue-moment"), { moment });
Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
