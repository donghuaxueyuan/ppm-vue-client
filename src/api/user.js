import request from '@/utils/request'


export function getUserList(data) {
    return request({
        url: '/api/users/',
        method: 'get',
        data
    })
}

export const getUserInfo = params => request({
    url: '/api/getuserinfo',
    method: 'get',
    params
})


export function saveUser(data) {
    return request({
        url: '/api/users/',
        method: 'post',
        data
    })
}

export function updateUser(data) {
    return request({
        url: '/api/users/' + data.id,
        method: 'put',
        data
    })
}

export function delUser(data) {
    return request({
        url: '/api/users/' + data.id,
        method: 'delete',
        data
    })
}

export function login(data) {
    return request({
        url: '/api/login',
        method: 'post',
        data
    })
}

export function logout() {
    return request({
        url: '/api/logout',
        method: 'delete'
    })
}

